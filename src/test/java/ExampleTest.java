import org.junit.Test;

public class ExampleTest {

	@Test(expected = IllegalArgumentException.class)
	public void testTryCatch() throws Exception {
		initialize();
		// duzo kodu

		throwsIAE();
	}

	private static void throwsIAE() {
		throw new IllegalArgumentException();
	}

	private static void initialize() {
		throw new IllegalArgumentException();
	}
}
