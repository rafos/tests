import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserTest {

	@Test
	public void shouldReturnDefaults() throws Exception {
		User user = new User();

		assertEquals(User.defaultLogin, user.getLogin());
		assertEquals(User.defaultPassword, user.getPassword());
	}

	@Test
	public void shouldReturnGivenLoginAndDefaultPassword() throws Exception {
		String expectedLogin = "Heniek";
		User user = new User(expectedLogin);

		assertEquals(expectedLogin, user.getLogin());
		assertEquals(User.defaultPassword, user.getPassword());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowsIAEIfLoginIsNull() throws Exception {
		new User(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowsIAEIfLoginIsEmpty() throws Exception {
		new User("");
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowsIAEIfLoginIsNullAndPasswordAreOk() throws Exception {
		new User(null, "haslo", "haslo");
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowsIAEIfLoginIsEmptyAndPasswordAreOk() throws Exception {
		new User("", "haslo", "haslo");
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowsIAEIfLoginIsOkAndPasswordIsNull() throws Exception {
		new User("heniek", null, "haslo");
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowsIAEIfLoginIsOkAndPasswordIsEmpty() throws Exception {
		new User("heniek", "", "haslo");
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowsIAEIfLoginIsOkAndSecondPasswordIsNull() throws Exception {
		new User("heniek", "haslo", null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowsIAEIfLoginIsOkAndSecondPasswordIsEmpty() throws Exception {
		new User("heniek", "haslo", "");
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowsISEIfPasswordIsNotEqualsSecondPassword() throws Exception {
		new User("heniek", "haslo", "sraslo");
	}

	@Test
	public void shouldReturnProperLoginAndPassword() throws Exception {
		String expectedLogin = "Heniek";
		String password = "haslo";
		String expectedPassword = "*****";
		User user = new User(expectedLogin, password, password);

		assertEquals(expectedLogin, user.getLogin());
		assertEquals(expectedPassword, user.getPassword());
	}

	@Test
	public void shouldReturnHiddenPasswordIfIsNotDefault() throws Exception {
		String hiddenPassword = "*****";
		User user = new User("Heniek", "haslo", "haslo");

		assertEquals(hiddenPassword, user.getPassword());
	}
}
