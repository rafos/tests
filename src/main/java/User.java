public class User {

	static String defaultLogin = "<default login>";
	static String defaultPassword = "<default password>";

	private String login;
	private String password;

	public User() {
		this.login = defaultLogin;
		this.password = defaultPassword;
	}

	public User(String login) {
		if (login == null || login.isEmpty()) {
			throw new IllegalArgumentException();
		}
		this.login = login;
		this.password = defaultPassword;
	}

	public User(String login, String password, String secondPassword) {
		if (login == null || login.isEmpty()) {
			throw new IllegalArgumentException();
		}
		if (password == null || password.isEmpty()) {
			throw new IllegalArgumentException();
		}
		if (secondPassword == null || secondPassword.isEmpty()) {
			throw new IllegalArgumentException();
		}

		if (!password.equals(secondPassword)) {
			throw new IllegalStateException();
		}
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		if (!defaultPassword.equals(password)) {
			String hiddenPassword = "";
			for (int i = 0; i < password.length(); i++) {
				hiddenPassword += "*";
			}
			return hiddenPassword;
		}

		return password;
	}
}
